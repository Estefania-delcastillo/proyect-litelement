import { LitElement, html } from "lit-element";

class DataManager extends LitElement {
  static get properties() {
    return {
      people: { type: Array },
    };
  }

  constructor() {
    super();

    this.people = [
      {
        name: "Estefania",
        yearsInCompany: 27,
        photo: {
          src: "./img/avatar.jpeg",
          alt: "Estefania",
        },
        profile: "Lorem ipsum dolor sit amet",
      },
      {
        name: "Aitor",
        yearsInCompany: 3,
        photo: {
          src: "../img/avatar1.jpeg",
          alt: "Aitor",
        },
        profile: "hello aitor como estas ",
      },
      {
        name: "Samira",
        yearsInCompany: 13,
        photo: {
          src: "./img/avatar.jpeg",
          alt: "Samira",
        },
        profile: "smaira es una chica muy extrovertida",
      },
      {
        name: "Hisam",
        yearsInCompany: 9,
        photo: {
          src: "./img/avatar1.jpeg",
          alt: "Hisam",
        },
        profile: "hisam es un chico muy malote",
      },
      {
        name: "Omar",
        yearsInCompany: 8,
        photo: {
          src: "./img/avatar1.jpeg",
          alt: "Omar",
        },
        profile: "Omar es muy inteligente ",
      },
    ];
  }

  updated(changeProperties) {
    console.log("updated");

    if (changeProperties.has("people")) {
      console.log("Ha cambiado el valor de la propiedad people");

      this.dispatchEvent(
        new CustomEvent("people-data-updated", {
          detail: {
            people: this.people,
          },
        })
      );
    }
  }
}

customElements.define("data-manager", DataManager);
